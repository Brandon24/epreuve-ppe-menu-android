package com.database.elieself;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.view.View;

import com.date.elieself.CustomDate;
import com.model.elieself.Menu;
import com.model.elieself.Produit;
import com.model.elieself.Repas;

import java.util.ArrayList;
import java.util.List;


/**
 * The type Database helper.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 5;
    private static final String DATABASE_NAME = "frigoDuLycee.db";
    /**
     * The constant TABLE_MENU.
     */
    protected static final String TABLE_MENU = "menu";

    /**
     * Instantiates a new Database helper.
     *
     * @param context the context
     */
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TABLE_MENU = "Create  TABLE if not exists  menu(" +
                "  `semaine` text NOT NULL, " +
                "  `jour` INTEGER NOT NULL  , " +
                "  `type_produit` text NOT NULL, " + // accompagnement entree dessert
                "  `type_repas` text NOT NULL ," + // midi ou soir
                "  `designation` text NOT NULL , " +
                "  `description` text," +
                "  `bio` INTEGER DEFAULT 0," +
                "  `maison` INTEGER DEFAULT 0," +
                "  `local` INTEGER DEFAULT 0," +
                "  `frais` INTEGER DEFAULT 0," +
                " PRIMARY KEY(semaine,jour,type_repas,designation) ON CONFLICT IGNORE " +
                ")";

        Log.i("OLAAA", "OnCREATE DE DATABASE HELPER");
        db.execSQL(CREATE_TABLE_MENU);

    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS menu");

        onCreate(db);
    }

    // code to add the new contact




}