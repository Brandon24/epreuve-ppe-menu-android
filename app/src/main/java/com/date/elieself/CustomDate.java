package com.date.elieself;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * The type Custom date.
 */
public class CustomDate {

    /**
     * Gets monday.
     *
     * @return the monday
     */
    public static String getMonday() {
        Calendar maDate = new java.util.GregorianCalendar();
        // on recupere la date actuelle
        Date todayDate = new Date(Calendar.getInstance().getTimeInMillis());
        maDate.setTime(todayDate);
        // On se positionne sur le Lundi de la semaine courante
        maDate.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");


        return ft.format(maDate.getTime());
    }

    /**
     * Get monday and friday string.
     *
     * @return the string
     */
/*

    Recupere la date du jour sous le format
     22 au 24 Mai

 */
    public static String getMondayAndFriday(){
        String dateDay = "";
        try {

            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
            cal.clear(Calendar.MINUTE);
            cal.clear(Calendar.SECOND);
            cal.clear(Calendar.MILLISECOND);
            cal.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);

            SimpleDateFormat ddd = new SimpleDateFormat("DD-YY");
            String friday = new SimpleDateFormat().format(cal.getTime());

            cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            String monday  = new SimpleDateFormat().format(cal.getTime());

            dateDay = "Menu du " + monday + " au " + friday;


        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateDay;
    }

    /**
     * Gets current day.
     *
     * @return the current day
     */
    public static String getCurrentDay() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        switch (day) {
            case Calendar.MONDAY:
                return "lundi";
            case Calendar.TUESDAY:
                return "mardi";
            case Calendar.WEDNESDAY:
                return "mercredi";
            case Calendar.THURSDAY:
                return "jeudi";
            case Calendar.FRIDAY:
                return "vendredi";
             default:
                 return "lundi";
        }
    }


}
