package com.model.elieself;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Repas.
 */
public class Repas {

    private List<Produit> produits;
    private String jour;
    private String typeRepas; // midi ou soir

    /**
     * Instantiates a new Repas.
     *
     * @param produits  the produits
     * @param jour      the jour
     * @param typeRepas the type repas
     */
    public Repas(List<Produit> produits, String jour,String typeRepas) {
        this.produits = produits;
        this.jour = jour;
        this.typeRepas = typeRepas;
    }

    /**
     * Instantiates a new Repas.
     */
    public Repas(){
        produits = new ArrayList<>();
    }

    /**
     * Add produit.
     *
     * @param produit the produit
     */
    public void addProduit(Produit produit){
        this.produits.add(produit);
    }

    /**
     * Sets jour.
     *
     * @param jour the jour
     */
    public void setJour(String jour) {
        this.jour = jour;
    }

    /**
     * Sets type repas.
     *
     * @param typeRepas the type repas
     */
    public void setTypeRepas(String typeRepas) {
        this.typeRepas = typeRepas;
    }

    /**
     * Gets produit.
     *
     * @return produit List
     */
    public List<Produit> getProduit() {
        return produits;
    }

    /**
     * Gets jour.
     *
     * @return the jour
     */
    public String getJour() {
        return jour;
    }

    /**
     * Gets type repas.
     *
     * @return the type repas
     */
    public String getTypeRepas() {
        return typeRepas;
    }

    @Override
    public String toString() {
        return "Repas{" +
                "produitss=" + produits +
                ", jour=" + jour +
                '}';
    }
}
