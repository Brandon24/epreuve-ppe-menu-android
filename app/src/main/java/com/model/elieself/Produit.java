package com.model.elieself;

/**
 * The type Produit.
 */
public class Produit {
    private String designation;
    private String typeProduit; // entree plat desset accompagnement
    private String description;
    private int bio; // 1 le produit est bio sinon il ne l'est pas
    private int maison;
    private int local;
    private int frais;


    /**
     * Instantiates a new Produit.
     *
     * @param designation nom du produit
     * @param typeProduit correspond a entree plat dessert
     * @param description correspond aux informations le concernant
     * @param bio         the bio
     * @param maison      the maison
     * @param local       the local
     * @param frais       the frais
     */
    public Produit( String designation, String typeProduit, String description, int bio, int maison, int local, int frais) {
        this.designation = designation;
        this.typeProduit = typeProduit;
        this.description = description;
        this.bio = bio;
        this.maison = maison;
        this.local = local;
        this.frais = frais;
    }

    /**
     * Instantiates a new Produit.
     */
    public Produit(){

    }

    /**
     * Gets designation.
     *
     * @return the designation
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * Gets type produit.
     *
     * @return the type produit
     */
    public String getTypeProduit() {
        return typeProduit;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets bio.
     *
     * @return the bio
     */
    public int getBio() {
        return bio;
    }

    /**
     * Gets maison.
     *
     * @return the maison
     */
    public int getMaison() {
        return maison;
    }

    /**
     * Gets local.
     *
     * @return the local
     */
    public int getLocal() {
        return local;
    }

    /**
     * Gets frais.
     *
     * @return the frais
     */
    public int getFrais() {
        return frais;
    }

    /**
     * Sets designation.
     *
     * @param designation the designation
     */
    public void setDesignation(String designation) {
        this.designation = designation;
    }


    @Override
    public String toString() {
        return "Produit{" +
                "designation='" + designation + '\'' +
                ", typeProduit='" + typeProduit + '\'' +
                ", description='" + description + '\'' +
                ", bio=" + bio +
                ", maison=" + maison +
                ", local=" + local +
                ", frais=" + frais +
                '}';
    }
}
