package com.elievinet.elieself;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.customWidget.elieself.ListViewAdapter;
import com.dao.MenuDAO;
import com.dao.ProduitDAO;
import com.database.elieself.DatabaseHelper;
import com.date.elieself.CustomDate;
import com.model.elieself.Menu;
import com.model.elieself.Produit;
import com.parser.JsonParser;

import java.util.ArrayList;
import java.util.List;

/**
 * Cette classe a pour but de récuperer les données du menu provenant du JSON et
 * de les afficher dans une ListView
 * Nous implémentons  l'interface OnItemSelectedListener pour pouvoir savoir quel jour choisit
 * l'utilisateur.
 */
public class MainActivity extends Activity implements AdapterView.OnItemSelectedListener {
    private ArrayList<Produit> listDesProduits = new ArrayList<>();
    private ArrayAdapter<CharSequence> adapterSpinner;
    private ListViewAdapter adapter;
    private ProduitDAO produitDAO;
    private MenuDAO menuDAO;
    private ListView listView;
    private String urlData = "http://172.18.67.1/ppeMenu/php/getmenu2.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        menuDAO = new MenuDAO(this);
        produitDAO = new ProduitDAO(this);
        listView = findViewById(R.id.imagesListView);

        TextView textViewSemaine = (TextView) findViewById(R.id.textView3);
        textViewSemaine.setText(CustomDate.getMondayAndFriday());
        Spinner spinner = findViewById(R.id.spinner);

        adapter = new ListViewAdapter(this,R.layout.custom_layout_list, listDesProduits);

        adapterSpinner = ArrayAdapter.createFromResource(this,
                R.array.day_week, android.R.layout.simple_spinner_item);
        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapterSpinner);

        // Définition Evenement et attribution jour
        spinner.setOnItemSelectedListener(this);

// Request a string response from the provided URL.
        RequestQueue queue = Volley.newRequestQueue(this);


        Log.i("aaa","AVANT JSON");

        StringRequest jsonObjectRequest = new StringRequest(
                Request.Method.POST, urlData,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        String data =  response;
                        List<Menu> menuList = new ArrayList<>();
                        menuList = JsonParser.getMenusFromJson(response.toString(),menuList);

                        Log.i("FIN" ,menuList.toString());
                        menuDAO.addMenuToDatabase(menuList);
                        updateListProduit(CustomDate.getCurrentDay());

                        Log.i("SIZE ARRAYLIST","" +listDesProduits.size());

                        listView.setAdapter(adapter);

                        Log.i("OLAAA","INSERTION EN BASE REUSSIE");

                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        // Do something when error occurred
                        Log.i("ERROR","a : "+error.getMessage());
                        error.printStackTrace();
                        Log.i("EROR",error.toString());

                    }


                }


        );

// Add the request to the RequestQueue.
       queue.add(jsonObjectRequest);
    }


    /**
     * Update list produit.
     *
     * @param jourVoulu est le jour cu l'on va récupérer les données du menu
     */
    public void updateListProduit(String jourVoulu){
        /**
         Aprés avoir reçu les données  du json et les avoir insérer dans la base
         on récupére les données de la base pour les injecter dans notre arraylist
         on va d'abord injecter le produit du midi
         On injecte un faux menu du a la contrainte de notre interface avec le listview
         Qui va nous permettre d'afficher l'entete DINER avant les résultats
         aprés on va injecter le repas du soi*/
        Produit produitDuMidi = new Produit();
        Produit produitDuSoir = new Produit();
        produitDuSoir.setDesignation("DINER");
        produitDuMidi.setDesignation("DEJEUNER");
        listDesProduits.add(produitDuMidi);
        listDesProduits.addAll(produitDAO.getAllProduitForOneDayWithSpecificTypeProduitAndTypeRepas("dejeuner",jourVoulu,"entree"));
        listDesProduits.addAll(produitDAO.getAllProduitForOneDayWithSpecificTypeProduitAndTypeRepas("dejeuner",jourVoulu,"plat"));
        listDesProduits.addAll(produitDAO.getAllProduitForOneDayWithSpecificTypeProduitAndTypeRepas("dejeuner",jourVoulu,"accompagnement"));
        listDesProduits.addAll(produitDAO.getAllProduitForOneDayWithSpecificTypeProduitAndTypeRepas("dejeuner",jourVoulu,"dessert"));
        listDesProduits.add(produitDuSoir);
        listDesProduits.addAll(produitDAO.getAllProduitForOneDayWithSpecificTypeProduitAndTypeRepas("diner",jourVoulu,"entree"));
        listDesProduits.addAll(produitDAO.getAllProduitForOneDayWithSpecificTypeProduitAndTypeRepas("diner",jourVoulu,"plat"));
        listDesProduits.addAll(produitDAO.getAllProduitForOneDayWithSpecificTypeProduitAndTypeRepas("diner",jourVoulu,"accompagnement"));
        listDesProduits.addAll(produitDAO.getAllProduitForOneDayWithSpecificTypeProduitAndTypeRepas("diner",jourVoulu,"dessert"));
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Log.i("POSITION","" + position);
        String [] daysOfTheWeek= {"lundi","mardi","mercredi","jeudi","vendredi"};

        listDesProduits.clear();
        String jourVoulu = daysOfTheWeek[position];

        updateListProduit(jourVoulu);

        adapter.notifyDataSetChanged();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

